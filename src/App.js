import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Vinyl from './components/Vinyl'
import './App.css';

class App extends Component {
    state = {
        vinylData: undefined
    }

    getRandomVinyl = async () => {
        const request = "https://api.discogs.com/users/ausamerika/collection/folders/0/releases"
        try {
            const data = await fetch(request)
                .then((response) => response.json())
                .catch(error => { throw error });
            const max = data.releases.length
            const rand = Math.floor(Math.random() * max);
            const randomVinyl = data.releases[rand];
            this.setState({ vinylData: randomVinyl });
        } catch (e) {
            console.error(e);
        }
    }

    cleanUp = async () => this.setState({ vinylData: undefined });

    render() {
        const { vinylData } = this.state
        return (
            <div className="App">
                <header className="App-header">
                    <p>Gabriel Gagnon</p>
                    {
                        <Button className="Button" onClick={this.getRandomVinyl} variant="outlined" color="secondary">
                            Give me a random music
                        </Button>
                    }
                    {
                        vinylData &&
                        <Button className="Button" onClick={this.cleanUp} variant="outlined" color="primary" style={{ marginTop: 25 }}>
                            Clean up
                        </Button>
                    }
                    {
                        vinylData &&
                        <Vinyl data={vinylData} />
                    }
                </header>
            </div>
        );
    }
}

export default App;
