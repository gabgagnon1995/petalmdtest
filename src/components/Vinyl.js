import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import './../App.css';
class Vinyl extends Component {
    render() {
        const { artists, title, year } = this.props.data.basic_information;
        const artistsList = artists.map((artist, index) => (
            <div key={index}>
                <h4>{artist.name}</h4>
            </div>
        ));

        return (
            <div className="Vinyl">
                <div>
                    <h3>Artists: </h3>
                    <Grid style={styles.artists} container spacing={8} direction='column'>
                        {artistsList}
                    </Grid>
                </div>
                <div>
                    <h3>Information: </h3>
                    <div>
                        <h5>Title: {title}</h5>
                        <h5>Year: {year}</h5>
                    </div>
                </div>
            </div>
        )
    }
}
Vinyl.propTypes = {
    data: PropTypes.object
};

const styles = {
    artists: {
        backgroundColor: "rgba(0,0,0,0.25)",
        borderRadius: 5,
    }
};

export default Vinyl;
